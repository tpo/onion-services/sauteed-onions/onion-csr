module sauteed-onions.org/onion-csr

go 1.18

require (
	github.com/cretz/bine v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20221010152910-d6f0a8c073c2 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
