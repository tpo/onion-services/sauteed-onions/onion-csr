package options

import (
	"fmt"
	"os"
	"reflect"
	"strings"
	"syscall"
	"testing"
)

func TestNew(t *testing.T) {
	setup(t)
	defer teardown(t)

	none := Options{}
	ok := Options{
		HSDir:   "/path/to/hsdir",
		CANonce: make([]byte, 10),
		caNonce: "00000000000000000000",
	}
	for _, table := range []struct {
		desc string
		args string
		want Options
	}{
		{"no options", "", none},
		{"unknown option --extra", "--extra value", none},
		{"unknown extra value", "extra", none},
		{"help option", "-h", none},
		{"missing --hs-dir", fmt.Sprintf("-n %x", ok.CANonce), none},
		{"missing --ca-nonce", fmt.Sprintf("-d %s", ok.HSDir), none},
		{"invalid --ca-nonce", fmt.Sprintf("-d %s -n 0q", ok.HSDir), none},
		{"valid", fmt.Sprintf("-d %s -n %x", ok.HSDir, ok.CANonce), ok},
		{"valid", fmt.Sprintf("--hs-dir %s --ca-nonce %x", ok.HSDir, ok.CANonce), ok},
	} {
		opts, err := New("onion-csr", strings.Split(table.args, " "))
		if got, want := err != nil, table.desc != "valid"; got != want {
			t.Errorf("%s: got error %v but wanted %v: %v", table.desc, got, want, err)
		}
		if err != nil {
			continue
		}
		if got, want := opts, table.want; !reflect.DeepEqual(got, want) {
			t.Errorf("%s: got options\n%v\nbut wanted\n%v\n", table.desc, got, want)
		}
	}
}

func setup(t *testing.T) {
	t.Helper()
	var err error
	if os.Stderr, err = os.CreateTemp("", "onion-csr@rgdd.se"); err != nil {
		t.Fatalf("create temp file: %v", err)
	}
}

func teardown(t *testing.T) {
	t.Helper()
	if err := os.Remove(os.Stderr.Name()); err != nil {
		t.Fatalf("remove temp file: %v", err)
	}
	os.Stderr = os.NewFile(uintptr(syscall.Stderr), "/dev/stderr")
}
