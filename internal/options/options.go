// Package options provides parsing of user-provided onion-csr options
package options

import (
	"encoding/hex"
	"flag"
	"fmt"
	"os"
)

const usage = `Usage:

  onion-csr -h
  onion-csr -d HS_DIR -n NONCE

Options:

  -h, --help:      Output usage message and exit
  -d, --hs-dir:    Path to hidden service directory
  -n, --ca-nonce:  Nonce provided by a certificate authority in hex
`

// Options is a collection of onion-csr options
type Options struct {
	HSDir   string
	CANonce []byte

	caNonce string
}

// New parses user-supplied onion-csr options
func New(cmd string, args []string) (opts Options, err error) {
	fs := flag.NewFlagSet(cmd, flag.ContinueOnError)
	fs.Usage = func() { fmt.Fprintf(os.Stderr, "%s", usage) }
	stringOpt(fs, &opts.HSDir, "hs-dir", "d", "")
	stringOpt(fs, &opts.caNonce, "ca-nonce", "n", "")
	if err = fs.Parse(args); err != nil {
		return opts, err
	}

	if opts.HSDir == "" {
		return opts, fmt.Errorf("-d, --hs-dir: must not be an empty string")
	}
	if opts.CANonce, err = hex.DecodeString(opts.caNonce); err != nil {
		return opts, fmt.Errorf("-n, --ca-nonce: %v", err)
	}
	if len(opts.CANonce) == 0 {
		return opts, fmt.Errorf("-n, --ca-nonce: must not be empty string")
	}
	return opts, err
}

func stringOpt(fs *flag.FlagSet, opt *string, short, long, value string) {
	fs.StringVar(opt, short, value, "")
	fs.StringVar(opt, long, value, "")
}
