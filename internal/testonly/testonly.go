// Package testonly provides common functions used to setup tests
package testonly

import (
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"encoding/hex"
	"log"
	"testing"
)

// RSAPriv generates a new RSA key
func RSAPriv(t *testing.T) *rsa.PrivateKey {
	t.Helper()
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Fatal(err)
	}
	return priv
}

// Ed25519Priv creates a new Ed25519 private key from a seed
func Ed25519Priv(t *testing.T, seed string) ed25519.PrivateKey {
	t.Helper()
	b := DecodeHex(t, seed)
	if len(b) != ed25519.SeedSize {
		log.Fatalf("invalid private key size: %d", len(b))
	}
	return ed25519.NewKeyFromSeed(b)
}

// DecodeHex decodes a hex-encoded string
func DecodeHex(t *testing.T, s string) []byte {
	t.Helper()
	b, err := hex.DecodeString(s)
	if err != nil {
		log.Fatal(err)
	}
	return b
}
