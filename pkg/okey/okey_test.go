package okey

import (
	"bytes"
	"os"
	"testing"

	bed25519 "github.com/cretz/bine/torutil/ed25519"
)

func TestNewFromHSDir(t *testing.T) {
	if _, err := NewFromHSDir("no-such-directory"); err == nil {
		t.Errorf("succeeded to read non-existing directory")
	}

	want := bed25519.PublicKey(hsPubFromFile(t, "testonly"))
	if s, err := NewFromHSDir("testonly"); err != nil {
		t.Errorf("failed reading valid directory: %v", err)
	} else if got, ok := s.Public().(bed25519.PublicKey); !ok {
		t.Errorf("wrong key type: %T", s.Public())
	} else if !bytes.Equal(got, want) {
		t.Errorf("got public key\n%x\nbut wanted\n%x", got, want)
	}
}

func TestNew(t *testing.T) {
	for _, table := range []struct {
		desc string
		priv []byte
		want bed25519.PublicKey
	}{
		{"invalid: key size", make([]byte, 95), nil},
	} {
		_, err := New(table.priv)
		if got, want := err != nil, table.desc != "valid"; got != want {
			t.Errorf("%s: got error %v but wanted %v: %v", table.desc, got, want, err)
		}
	}
}

func hsPubFromFile(t *testing.T, dir string) []byte {
	t.Helper()
	b, err := os.ReadFile(dir + "/hs_ed25519_public_key")
	if err != nil {
		t.Fatal(err)
	}
	if len(b) != 64 {
		t.Fatalf("invalid public key file size: %d", len(b))
	}
	return b[32:64]
}
