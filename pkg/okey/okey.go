// Package okey provides access to onion service private keys
package okey

import (
	"crypto"
	"fmt"
	"os"

	bed25519 "github.com/cretz/bine/torutil/ed25519"
)

// New parses the content of Tor's hs_ed25519_secret_key file by interpretting
// bytes 32..96 as the 64-byte expanded seed.  For reference, see:
// https://gitlab.torproject.org/tpo/core/tor/-/blob/main/src/feature/keymgt/loadkey.c#L379
func New(b []byte) (crypto.Signer, error) {
	if len(b) != 96 {
		return nil, fmt.Errorf("invalid key file size: %d", len(b))
	}
	return bed25519.PrivateKey(b[32:96]), nil
}

// NewFromHSDir reads and parses the hs_ed25519_secret_key file in a given directory
func NewFromHSDir(dir string) (crypto.Signer, error) {
	b, err := os.ReadFile(dir + "/hs_ed25519_secret_key")
	if err != nil {
		return nil, err
	}
	return New(b)
}
