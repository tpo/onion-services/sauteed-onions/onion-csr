package ocsr

import (
	"crypto"
	"testing"

	"sauteed-onions.org/onion-csr/internal/testonly"
)

func TestNew(t *testing.T) {
	// test params that HARICA were able to verify successfully with address
	// b3ttntbojgjyj54nbjrvcabqftyti5m6p6vebkqmwvbbxydvevkhp3ad.onion
	testPriv := testonly.Ed25519Priv(t, "797134fa0f5667479138d868f258c9b81ef9a247fb2cfbfaec52e2afca0ff457")
	testCANonce := testonly.DecodeHex(t, "4865685837665A75714B655361367A6352493242")
	testApplicantNonce := testonly.DecodeHex(t, "00000000000000000000")
	testPEM := `-----BEGIN CERTIFICATE REQUEST-----
MIIBFzCBygIBADAAMCowBQYDK2VwAyEADuc2zC5Jk4T3jQpjUQAwLPE0dZ5/qkCq
DLVCG+B1JVSggZYwWgYJKoZIhvcNAQkOMU0wSzBJBgNVHREEQjBAgj5iM3R0bnRi
b2pnanlqNTRuYmpydmNhYnFmdHl0aTVtNnA2dmVia3Ftd3ZiYnh5ZHZldmtocDNh
ZC5vbmlvbjAgBgRngQwpMRgwFgQUSGVoWDdmWnVxS2VTYTZ6Y1JJMkIwFgYEZ4EM
KjEOMAwECgAAAAAAAAAAAAAwBQYDK2VwA0EALWQAfPUyaiGi5DriKQBijomZik+L
mEi8egO6VcgM2Q6RSajveWx5EImi3nQcU/vZ2NhdzYRyuiG1zYcj8SlgBA==
-----END CERTIFICATE REQUEST-----
`
	for _, table := range []struct {
		desc           string
		priv           crypto.Signer
		caNonce        []byte
		applicantNonce []byte
		want           string
	}{
		{"invalid: short nonce", testPriv, testCANonce, testonly.DecodeHex(t, "01020304050607"), ""},
		{"invalid: private key", testonly.RSAPriv(t), testCANonce, testApplicantNonce, ""},
		{"valid", testPriv, testCANonce, testApplicantNonce, testPEM},
	} {
		csr, err := New(table.priv, table.caNonce, table.applicantNonce)
		if got, want := err != nil, table.desc != "valid"; got != want {
			t.Errorf("%s: got error %v but wanted %v: %v", table.desc, got, want, err)
		}
		if err != nil {
			continue
		}
		if got, want := csr, table.want; got != want {
			t.Errorf("%s: got csr\n%s\nbut wanted\n%s", table.desc, got, want)
		}
	}
}
