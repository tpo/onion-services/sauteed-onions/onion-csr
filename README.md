# Onion CSR

A tool that generates Certificate Signing Requests (CSRs) for v3 `.onion`
addresses, see Appendix B(2) in the [CA/Browser Forum Baseline Requirements][].

[CA/Browser Forum Baseline Requirements]: https://cabforum.org/wp-content/uploads/CA-Browser-Forum-BR-1.7.4.pdf

## Status

Working prototype, please try it and report issues on IRC/Matrix or [GitLab][].

[GitLab]: https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/onion-csr

## Quick start

You will need a Go compiler (version 1.18 or later).  If it is not packaged as
part of your distribution, see [how to download and install Go][].

[how to download and install Go]: https://go.dev/doc/install

### Install

    $ go install sauteed-onions.org/onion-csr@latest

### Usage

    $ onion-csr -h
    Usage:

      onion-csr -h
      onion-csr -d HS_DIR -n NONCE

    Options:

      -h, --help:      Output usage message and exit
      -d, --hs-dir:    Path to hidden service directory
      -n, --ca-nonce:  Nonce provided by a certificate authority in hex

Note that the above UI is intentionally backwards-compatible with HARICA's
[onion-csr][] tool.  The main difference is that HARICA's tool is in Ruby+C.

[onion-csr]: https://github.com/HARICA-official/onion-csr

### Example

Below is an example for the CA-provided nonce `0123456789` and an onion service
that has its `hs_ed25519_secret_key` file stored in `/var/lib/tor/mysite`.

    $ onion-csr -n 0123456789 -d /var/lib/tor/mysite
    -----BEGIN CERTIFICATE REQUEST-----
    MIIBCDCBuwIBADAAMCowBQYDK2VwAyEABu7tFhSrDtml3vbLzXehfJVKOfyEae9e
    mRMsUa+FMi2ggYcwWgYJKoZIhvcNAQkOMU0wSzBJBgNVHREEQjBAgj5hM3hvMmZx
    dXZtaG50am82NjNmNDI1NWJwc2t1dW9wNHFydTY2eHV6Y213ZmRsNGZnaXd0M3Vp
    ZC5vbmlvbjARBgRngQwpMQkwBwQFASNFZ4kwFgYEZ4EMKjEOMAwECswNE8rAq09k
    Tv8wBQYDK2VwA0EAzTog3GjzGuzmlpDViTB35ZQiISpKfHWpTA4/F00FCrEapEu6
    Eec4yJx9kNXOBxa1pZxKF6DchnmfWM3YsOqaDg==
    -----END CERTIFICATE REQUEST-----

If you'd like to view the above in ASCII, save it as `csr.pem` and try:

    $ openssl req -in csr.pem -noout -text
    Certificate Request:
        Data:
            Version: 1 (0x0)
            Subject:
            Subject Public Key Info:
                Public Key Algorithm: ED25519
                    ED25519 Public-Key:
                    pub:
                        06:ee:ed:16:14:ab:0e:d9:a5:de:f6:cb:cd:77:a1:
                        7c:95:4a:39:fc:84:69:ef:5e:99:13:2c:51:af:85:
                        32:2d
            Attributes:
                2.23.140.41              :unable to print attribute
                2.23.140.42              :unable to print attribute
                Requested Extensions:
                    X509v3 Subject Alternative Name:
                        DNS:a3xo2fquvmhntjo663f4255bpskuuop4qru66xuzcmwfdl4fgiwt3uid.onion
        Signature Algorithm: ED25519
        Signature Value:
            99:dd:d4:0d:b5:27:0a:17:ab:73:45:a7:84:ff:cb:87:c1:33:
            75:7d:a1:e2:7b:29:a9:d1:08:a8:5d:1c:91:26:01:e5:b7:92:
            c0:1e:b5:14:32:85:53:b4:04:7c:6c:22:95:21:01:6d:3b:16:
            48:23:c4:ca:80:7b:c3:c8:07:0d

## Contact

  - GitLab: [issue board][]
  - IRC: room #sauteed-onions at OFTC.net
  - Matrix: room [#sauteed-onions][] (which is bridged with IRC)

[issue board]: https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/onion-csr/-/issues
[#sauteed-onions]: https://app.element.io/#/room/#sauteed-onions:matrix.org

## Licence

BSD 2-Clause License
