package main

import (
	"crypto/rand"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"

	"sauteed-onions.org/onion-csr/internal/options"
	"sauteed-onions.org/onion-csr/pkg/ocsr"
	"sauteed-onions.org/onion-csr/pkg/okey"
)

const entropyBytes = 10 // 80 bits

func main() {
	opts, err := options.New(os.Args[0], os.Args[1:])
	if err != nil {
		if errors.Is(err, flag.ErrHelp) {
			os.Exit(0)
		}

		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	var applicantNonce [entropyBytes]byte
	if _, err := io.ReadFull(rand.Reader, applicantNonce[:]); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}
	priv, err := okey.NewFromHSDir(opts.HSDir)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}
	csr, err := ocsr.New(priv, opts.CANonce, applicantNonce[:])
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "%s", csr)
}
